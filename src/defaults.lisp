(in-package #:avesh)

(defcommandhook exec () (command arguments next)
  (when (= (length arguments) 0)
    (error "Not enough arguments to exec."))
  (run-exec arguments))

(defcommandhook exit () (command arguments next)
  (when (> (length arguments) 1)
    (error "Too many arguments to exit."))
  (uiop:quit
   (if (first arguments)
       (parse-integer (first arguments))
       0)))

(defvar *children* nil)

(defhook command (:weight 1000) (command arguments next)
  (when (> (length command) 0)
    (let ((pid (sb-posix:fork)))
      (cond
        ((<= pid -1) (error (format nil "Failed to start ~A" command)))
        ((= pid 0) (handler-case
		       (run-exec (append (list command) arguments))
		     (os-error (e)
		       (format *error-output* "avesh: ~a: ~a~%" command (strerror e))
		       (uiop:quit (errno e) nil))
		     (error (e)
		       (format *error-output* "avesh: ~a~%" e)
		       (uiop:quit -1 nil))))
        (t (progn
	     (push pid *children*)
	     (multiple-value-bind (pid status)
		(sb-posix:waitpid pid 0)
              (remove pid *children*)
              status))))))
  (funcall next command arguments))

(cffi:defcvar *errno* :int)

(defun empty-string-p (string)
  (string= string ""))

(define-condition os-error (error)
  ((errno :initarg :errno :reader errno)
   (strerror :initarg :strerror :reader strerror)))

(export 'run-exec)
(defun run-exec (command)
  (cffi:with-foreign-objects ((argv :string (1+ (length command))))
    (dotimes (i (length command))
      ;; Some elements can be symbols or numbers
      (setf (cffi:mem-aref argv :string i) (format nil "~A" (nth i command))))
    ;; Last element must be NULL pointer
    (setf (cffi:mem-aref argv :string (length command)) (cffi:null-pointer))
    (cffi:with-foreign-string (cmd (format nil "~A" (first command)))
      (when (< (cffi:foreign-funcall "execvp"
                                     :pointer cmd
                                     :pointer argv
                                     :int)
               0)
	(error 'os-error
	       :errno *errno*
	       :strerror (cffi:foreign-funcall "strerror" :int *errno* :string))))))
