(defpackage #:avesh
  (:use #:cl)
  (:local-nicknames (#:reader #:com.informatimago.common-lisp.lisp-reader.reader)))

(defpackage #:avesh-user
  (:use #:cl #:avesh))
