(in-package #:avesh)

(defvar *hooks* (make-hash-table))

(export 'defhook)
(defmacro defhook (name options vars &body body)
  `(progn
     (defun ,name ,vars
       (when *debug* (format t "DEBUG: Hook ~A called.~%" ',name))
       (unwind-protect
	    (progn ,@body)
	 (when *debug* (format t "DEBUG: Hook ~A finished.~%" ',name))))
     (setf (gethash ',name *hooks*)
           (list
	    :name ',name
	    :function (function ,name)
	    :weight ,(or (getf options :weight) 0)
	    :type ,(or (getf options :type) :command)))))

(export 'defcommandhook)
(defmacro defcommandhook (name options vars &body body)
  `(defhook ,name ,options ,vars
     (unless (and
	      (stringp ,(first vars))
	      (string= ,(first vars) (string-downcase (symbol-name ',name))))
       (return-from ,name (funcall ,(third vars) ,(first vars) ,(second vars))))
     ,@body))

(defun ordered-hooks (hooks type)
  "Returns a list of all the hooks of a type, ordered by weight."
  (sort (remove-if-not (lambda (hook)
			 (eq (getf (gethash (getf hook :name) hooks) :type) type))
		       (hash-table-values hooks))
	(lambda (h1 h2)
	  (< (getf (gethash (getf h1 :name) hooks) :weight)
	     (getf (gethash (getf h2 :name) hooks) :weight)))))

(defun hash-table-values (hash-table)
  (let ((values nil))
    (maphash (lambda (k v)
               (declare (ignore k))
               (push v values))
             hash-table)
    values))
