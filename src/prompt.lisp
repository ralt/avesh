(in-package #:avesh)

(defun default-prompt-callback ()
  (format nil
          "[~A@~A ~A]~A "
          (slot-value (sb-posix:getpwuid (sb-posix:getuid)) 'sb-posix::name)
          (uiop:hostname)
          (first (last (pathname-directory (uiop:getcwd))))
          (if (= (sb-posix:getuid) 0) "#" "$")))

(export '*prompt-callback*)
(defvar *prompt-callback* #'default-prompt-callback
  "To display the prompt, the function stored in this variable
is used. It must return a string.")
