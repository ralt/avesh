(in-package #:avesh.time-command)

(export '*time-after*)
(defvar *time-after* nil "Whether to print the time after the command as well. Defaults nil.")

(defun print-time ()
  (multiple-value-bind (seconds minutes hours)
      (get-decoded-time)
    (format t "[~2,'0D:~2,'0D:~2,'0D]~%" hours minutes seconds)))

(defhook time-command (:weight -1000) (command arguments next)
  (when (or (not (eq command nil))
	    (not (eq arguments nil)))
    (print-time))
  (unwind-protect
       (funcall next command arguments)
    (when (and (or (not (eq command nil))
		   (not (eq arguments nil)))
	       *time-after*)
      (print-time))))
