(in-package #:avesh.history)

(defhook read-history (:type :startup) (next)
  (cffi:foreign-funcall "read_history" :pointer (cffi:null-pointer) :int)
  (funcall next))

(defhook write-history (:weight 100) (command arguments next)
  (cffi:foreign-funcall "write_history" :pointer (cffi:null-pointer) :int)
  (funcall next command arguments))

(defcommandhook history () (command arguments next)
  (when (> (length arguments) 0)
    (error "Too many arguments to history."))
  (format t "~A" (uiop:read-file-string
                  (merge-pathnames #p ".history"
                                   (user-homedir-pathname)))))
