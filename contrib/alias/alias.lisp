(in-package #:avesh.alias)

(defvar *aliases* (make-hash-table))

(defun hash-table-keys (hash)
  (let ((keys nil))
    (maphash (lambda (key value)
               (declare (ignore value))
               (push key keys))
             hash)
    keys))

(defhook expand-alias (:weight -500) (command arguments next)
  (funcall next
           (if (member command (hash-table-keys *aliases*))
               (gethash command *aliases*)
               command)
           arguments))

(defcommandhook alias () (command arguments next)
  (cond
    ((= (length arguments) 0) (list-aliases))
    ((= (length arguments) 1) (show-alias (first arguments)))
    ((= (length arguments) 2) (add-alias (first arguments) (second arguments)))
    (t (error "Too many arguments to alias."))))

(defun list-aliases ()
  (if (> (length (hash-table-keys *aliases*)) 0)
      (maphash (lambda (alias value)
                 (format t "Alias ~A: ~A~%" alias value))
               *aliases*)
      (format t "No alias.~%")))

(defun show-alias (alias)
  (multiple-value-bind (value presentp)
      (gethash alias *aliases*)
    (if presentp
        (format t "Alias ~A: ~A~%" alias value)
        (error (format nil "Alias ~A not found." alias)))))

(defun add-alias (alias value)
  (setf (gethash alias *aliases*) value))
